[Descripción Spark](./Descripcion_Spark.md)

Apache Spark:


Spark es una plataforma de código abierto para procesamiento paralelo en clusters. Está orientada a manejar grandes volúmenes de datos y ejecutar cómputo intensivo 
sobre ellos. Nos ofrece varias ventajas y reduce significativamente los tiempos de ejecución.
 
