Apache Hadoop es un framework de código abierto que permite el almacenamiento distribuido y 
el procesamiento de grandes conjuntos de datos en base a un hardware comercial. 
En otras palabras, Hadoop hace posible a las organizaciones obtener conocimiento rápidamente 
a partir de cantidades masivas de datos, estructurados y no estructurados, posicionándolas 
al nivel de las exigencias actuales de los mercados en términos de dinamismo y capacidad.

[Descripción Hadoop](./hadoop.md)
